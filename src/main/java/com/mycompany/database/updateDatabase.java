/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class updateDatabase {
     public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQL");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

//insert
        String sql = "UPDATE category SET cat_name =? WHERE cat_id=? ";
        try {
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, "MyCoffee");
            stat.setInt(2, 1);
            
            int status = stat.executeUpdate();
            //ResultSet Key = stat.getGeneratedKeys();
            //Key.next();
            //System.out.println(""+ Key.getInt(1));
            

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

//close data
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
    
}
